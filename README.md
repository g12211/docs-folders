## 使用文档目录

### 说明

本文介绍通过使用工具: [docsify](https://docsify.js.org/)（一个神奇的文档网站生成器）将编写好的markdown文件生成网站, 并部署到Gitlab上的过程.


### 1. 开发调试
* 1.1 [开发环境安装](#环境安装)
* 1.2 [环境安心](环境安心)
* [部署新版本](#部署新版本)


## 开发环境安装

1. `npm` 安装 `docsify-cli`

    ``` bash
        npm i docsify-cli -g
    ```

2. 克隆项目后，进入项目根目录, 通过运行 `docsify serve` 启动一个本地服务器，可以实时预览效果。默认访问地址 http://localhost:3000 。

    ``` bash
        cd ./docs-folders/
        docsify serve ./docs
    ```
3. 开发文档参考 [docsify官方文档](https://docsify.js.org/)



## 新版发布步骤
1. [归档当前最新版本](#归档当前最新版本)
    1. [文件归档](文件归档)
    2. [更新选择框](更新选择框)
2. [开发新版本](#开发新版本)


### 归档当前最新版本
#### 文件归档
1. 在 `docs/v` 目录下新建文件夹，命名为： `v + 归档版本号`, 如 `v3`
2. 将 `/docs` 目录下的当前版本涉及到的相关配置文件，markdown文件等所有内容拷贝到 `v3` 目录，完成所谓的归档

#### 更新选择框
1. 将 `docs/v` 目录下的所有已归档的版本下的 `index.html` 文件修改`html`的 `select`标签选项增加刚才归档的版本

    `例如: `

    ![imgs](/assets/selector.png)
2. 将 `v3` 目录中的 `select`标签中的 `option` 设置为 `selected="true"`
    ``` html
    <option value="/v/v3/index.html" selected="true">v3</option>
    ```

## 开发新版本
1. 修改 `docs` 目录下的文件完成内容增删改，参考 [docsify 教程](https://docsify.js.org/#/quickstart?id=writing-content)

    `例如: `
    - 可修改 `_sidebar.md` 文件，调整左侧导航栏
    - 可修改 `doc.html` 文件，调整文档页面
    - 可修改 `README.md` 文件，调整主页内容
    - 更多...

2. 代码测试验证通过后，将代码合并到 `master`分支
3. gitlab继承的ci将自动部署



## TODO
有不少需要优化的地方

- [ ] 自动脚本归档当前最新版本
- [ ] 实现选择框自动修改选项, 增加选项
- [ ] ...
