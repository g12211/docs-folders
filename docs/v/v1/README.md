<p align="center">
	<img alt="logo" src="logo.png" width="400">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">我是readme.md 文件 （v1）</h1>
<h5 align="center">专注研发xxx产品，专业提供xxxx解决方案</h5>

---

## 前言：️️

就其核心而言，HTML 是一种相当简单的、由不同元素组成的标记语言，它可以被应用于文本片段，使文本在文档中具有不同的含义（它是段落吗？它是项目列表吗？它是表格吗？），将文档结构化为逻辑块（文档是否有头部？有三列内容？有一个导航菜单？），并且可以将图片，影像等内容嵌入到页面中。本模块将介绍前两个用途，并且介绍一些 HTML 的基本概念和语法。

## 指南

本模块包含以下文章，它们将带你了解 HTML 的所有基本理论，并为你提供充分的机会来测试一些技能。


## 开源仓库 Star 趋势

<p class="un-dec-a-pre"></p>

[![github-chart](https://starchart.cc/dromara/sa-token.svg 'GitHub')](https://starchart.cc/dromara/sa-token)

